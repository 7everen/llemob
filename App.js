/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */


import React, {Component} from 'react';
import {StackNavigator} from 'react-navigation';


import { createStackNavigator } from 'react-navigation';
import MainScreen from "./src/screen/MainScreen";
import PageScreen from "./src/screen/PageScreen";
import ProfileScreen from "./src/screen/ProfileScreen";
import QuestScreen from "./src/screen/QuestScreen";
import TestScreen from "./src/screen/TestScreen";
import Test2Screen from "./src/screen/Test2Screen";
import DeckSwiper from "./src/screen/DeckSwiper";
import LoginScreen from "./src/screen/LoginScreen";
import DeckSwiperExample from "./src/screen/DeckSwiperExample";

const App = createStackNavigator({
    Main: { screen: MainScreen },
    Test: { screen: TestScreen },
    DeckSwiperExample: { screen: DeckSwiperExample },
    Quest: { screen: QuestScreen },
    LoginScreen: { screen: LoginScreen },
    DeckSwiper: { screen: DeckSwiper },
    Test2: { screen: Test2Screen },
    Page: { screen: PageScreen },
    Profile: { screen: ProfileScreen }
});

export default App;
