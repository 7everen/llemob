/** @format */

'use strict';

import {Alert, Image} from "react-native";

function alert(title, message) {
    Alert.alert(
        title,
        message,
        [
            {text: 'OK'},
        ],
        { cancelable: false }
    )
}

function warning(message) {
    this.alert("Warning", message);
}

function countWordsInPage(page, state){
    return page.sentences.reduce((acc, sentence) => acc + sentence.words.filter((word) => word.state === state).length, 0);
}

function wordsInPage(page){
    return page.sentences.flatMap(sentence => sentence.words);
}

async function getImageSize (source) {
    return new Promise((resolve) => {
        Image.getSize(source, (width, height) => {
            resolve({width: width, height: height});
        });
    })
}

const WORDS_PER_DAY = 20;
const LEARN_LEVELS = [
    {"period": 0, "divergence": 0, "fromRate": 0.0, "toRate": 0.2},
    {"period": 1, "divergence": 0, "fromRate": 0.2, "toRate": 0.4},
    {"period": 6, "divergence": 1, "fromRate": 0.4, "toRate": 0.6},
    {"period": 30, "divergence": 6, "fromRate": 0.6, "toRate": 0.8},
    {"period": 365, "divergence": 30, "fromRate": 0.8, "toRate": 1},
];

function mustLearn(pages) {
    let mustBeLearned = [];
    const canBeLearned = {};
    pages.forEach((page) => page.sentences.forEach((sentence) => sentence.words.forEach((word) => {
        let levelNow = LEARN_LEVELS.find((level) => level.fromRate <= word.rate && level.toRate > word.rate);
        if(levelNow){
            const timeHasPassed =  Date.now() - Date.parse(word.updated+"Z");
            const daysHavePassed = Math.floor(timeHasPassed/86400000);
            let daysLeft;
            if(levelNow.period-levelNow.divergence <= daysHavePassed){
                if(daysHavePassed > levelNow.period+levelNow.divergence){
                    const daysInLevel = levelNow.period+levelNow.divergence;
                    const minusLevels = daysInLevel?Math.floor(daysInLevel/levelNow.period):0;
                    let toLevel = LEARN_LEVELS.indexOf(levelNow) - minusLevels;
                    if(toLevel<0) toLevel = 0;
                    levelNow =  LEARN_LEVELS[toLevel];
                    daysLeft = 0;
                }else{
                    daysLeft = levelNow.period+levelNow.divergence - daysHavePassed;
                }
                if(!canBeLearned[daysLeft]) canBeLearned[daysLeft] = [];
                canBeLearned[daysLeft].push({
                    "word": {
                        "url": page.url,
                        "title": page.title,
                        "from": sentence.from,
                        "to": sentence.to,
                        "sentence": sentence.sentence,
                        "extra": sentence.extra,
                        "word": word.word,
                        "partOfSpeech": word.partOfSpeech,
                        "translation": word.translation,
                        "translit": word.translit,
                        "rate": levelNow.fromRate,
                        "updated": word.updated
                    },
                    "level": levelNow
                });
            }
        }
    })));

    Object.keys(canBeLearned).map(function(key) {
        if(key == 0){
            mustBeLearned = [...mustBeLearned, ...canBeLearned[key]];
        }else if(mustBeLearned.length < WORDS_PER_DAY){
            const needWords = WORDS_PER_DAY - mustBeLearned.length;
            if(needWords >= canBeLearned[key].length) {
                mustBeLearned = [...mustBeLearned, ...canBeLearned[key]];
            }else{
                mustBeLearned = [...mustBeLearned, ...canBeLearned[key].slice(0, needWords)];
            }
        }
    });

    return mustBeLearned;
}

function findNotLearnedWord(array, startFrom) {
    const arr = [...array.slice(startFrom), ...array.slice(0, startFrom)];
    const item = arr.find((item) => item.word.rate<item.level.toRate);
    return item;

}

function shuffleArray(array) {
    const clone = [...array];
    for (let i = clone.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [clone[i], clone[j]] = [clone[j], clone[i]];
    }
    return clone;
}

function compareItemAndShuffleArray(array, item) {
    let result = [...array, item];
    result = shuffleArray(result);
    return result;
}

const Utils = {
    alert,
    warning,
    wordsInPage,
    countWordsInPage,
    getImageSize,
    mustLearn,
    shuffleArray,
    findNotLearnedWord,
    compareItemAndShuffleArray
};

export default Utils;