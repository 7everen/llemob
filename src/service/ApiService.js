/** @format */

'use strict';

//const API_URL = 'http://trans-and-learn.us-east-2.elasticbeanstalk.com/';
const API_URL = 'http://trans-and-learn-env-int.us-east-2.elasticbeanstalk.com/';
//const API_URL = 'http://localhost.charlesproxy.com:8000/';

async function fetchSync(hash, syncFrom) {
    return fetch(API_URL+'sync', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            hash: hash,
            syncFrom: syncFrom,
            pages: []
        }),
    }).then((response) => response.json());
}

async function fetchProfile(email) {
    return fetch(API_URL+'profile', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            email: email
        }),
    }).then((response) => response.json());
}

async function fetchImage(word, lang) {
    return fetch(API_URL+'get-full-image', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            word: word,
            lang: lang,
        }),
    }).then((response) => response.json());
}

async function fetchFullWordInfo(hash, word, from, to, pos, translation) {
    return fetch(API_URL+'get-full-word-info', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            hash: hash,
            word: word,
            from: from,
            to: to,
            partOfSpeech: pos,
            translation: translation
        }),
    }).then((response) => response.json());
}



const ApiService = {
    fetchSync,
    fetchProfile,
    fetchImage,
    fetchFullWordInfo
};

export default ApiService;