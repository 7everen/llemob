import React, { Component } from "react";
import {View, Image, Dimensions} from 'react-native';
import Utils from "../service/Utils";

const win = Dimensions.get('window');
const padding = 10;

export default class ImageInCard extends Component {

    state = {
        cardHeight: 0
    };

    async componentDidMount(){
        const size = await Utils.getImageSize(this.props.base64);
        const imageH = Math.ceil((win.width-padding*2)/size.width*size.height);
        this.setState({
            cardHeight:imageH
        });
    }

    render() {
        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
                alignItems: 'stretch',
                marginHorizontal: padding,
                marginTop:padding,
                height: this.state.cardHeight}}>

                <Image source={{"uri":this.props.base64}}
                       style={{
                    flex: 1,
                    alignSelf: 'stretch',
                    width: win.width-padding*2,
                    borderRadius: 5
                }}
                       resizeMode={'contain'} />

            </View>
        );
    }

}