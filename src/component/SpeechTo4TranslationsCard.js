import React, { Component } from "react";
import {Text, View, ScrollView, TouchableOpacity} from 'react-native';
import ImageInCard from "./ImageInCard";
import SelectiveButtons from "./SelectiveButtons";
import OptionalButtons from "./OptionalButtons";
import Speech from 'react-native-speech';
import DefaultImages from "../service/DefaultImages";

export default class SpeechTo4TranslationsCard extends Component {

    state = {
        cardHeight: 0
    };

    async componentDidMount(){
        this.speech();
    }

    speech(){
        Speech.speak({
            text: this.props.data.word.word,
            voice: 'en-US',
            rate: 0.4
        })
            .then(started => {
                // Success code
            })
            .catch(error => {
                // Failure code
            });
    }


    render() {
        return (
            <ScrollView>
                <View style={{marginTop:40}}>

                    <TouchableOpacity
                        onPress={() => this.speech()}>
                        <ImageInCard base64={DefaultImages.SPEECH_IMAGE}/>
                    </TouchableOpacity>

                    <View>

                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            backgroundColor: 'white',
                            borderRadius: 5,
                            alignItems: 'stretch',
                            marginHorizontal: 10,
                            marginTop:5,
                            marginBottom: 5,
                            paddingVertical: 5
                        }}>

                            <Text style={{fontSize: 13, marginLeft: 10, color: 'gray'}}>
                                Choose correct translation
                            </Text>

                            <Text style={{fontSize: 17, marginLeft: 10, color: 'black'}}>
                                {this.props.data.word.word}
                            </Text>

                        </View>

                        <SelectiveButtons
                            falseValues={this.props.data.fullInfo.falseTranslationOptions}
                            value={this.props.data.word.translation}
                            onComplete={(result) => this.chose(result)}/>

                        <OptionalButtons onLearned={() => this.props.onNext(null, true)}/>

                    </View>

                </View>
            </ScrollView>

        );
    }

    chose(value) {
        if(value){
            this.props.onNext(value == this.props.data.word.translation);
        }

    }

}