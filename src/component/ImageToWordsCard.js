import React, { Component } from "react";
import {StyleSheet, Text, View, Image, ScrollView, TextInput} from 'react-native';
import {Button} from "react-native-elements";
import { Dimensions } from "react-native";
import Utils from "../service/Utils";
import LearnManager from "../manager/LearnManager";
import SelectiveInput from "./SelectiveInput";
import SelectiveButtons from "./SelectiveButtons";


const win = Dimensions.get('window');

const styles = StyleSheet.create({
    image: {
        flex: 1,
        alignSelf: 'stretch',
        width: win.width-20,
        borderRadius: 5
    },
    firstBlock: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white',
        borderRadius: 5,
        alignItems: 'stretch',
        marginHorizontal: 10,
        marginTop:5,
        marginBottom: 5,
        paddingVertical: 5
    }
});

export default class ImageToWordsCard extends Component {

    state = {
        cardHeight:0
    };

    async componentDidMount(){
        const size = await Utils.getImageSize(this.props.data.fullInfo.base64);
        const imageH = Math.ceil((win.width-20)/size.width*size.height);
        this.setState({
            cardHeight:imageH
        });
    }

    render() {
        return (
            <ScrollView>
                <View>
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        alignItems: 'stretch',
                        marginHorizontal: 10, marginTop:10, height: this.state.cardHeight}}>

                        <Image source={{"uri":this.props.data.fullInfo.base64}} style={styles.image} resizeMode={'contain'} />

                    </View>


                    {(() => {
                        switch (this.props.data.type){
                            case LearnManager.FULL_INFO: return this.fullInfoRender();
                            case LearnManager.TRANSLATION_TO_4WORDS: return this.translationTo4WordsRender();
                            case LearnManager.TRANSLATION_TO_WRITE: return this.translationToWriteRender();
                        }
                    })()}


                </View>
            </ScrollView>

        );
    }

    chose(value) {
        if(this.props.data.type == LearnManager.TRANSLATION_TO_WRITE ||
            this.props.data.type == LearnManager.TRANSLATION_TO_4WORDS) {
            if(value){
                this.props.onNext(value);
            }
        }

    }

    fullInfoRender() {
        return (
            <View>

                <View style={styles.firstBlock}>

                    <Text style={{fontSize: 13, marginLeft: 10, color: 'gray'}}>
                        {this.props.data.word.translation}
                    </Text>

                    <Text style={{fontSize: 17, marginLeft: 10, color: 'black'}}>
                        {this.props.data.word.word}
                    </Text>

                    <Text style={{fontSize: 13, marginLeft: 10, color: 'black'}}>
                        [{this.props.data.word.translit}]
                    </Text>

                </View>

                <View style={{
                    flex: 1,
                    flexDirection: 'column',
                    backgroundColor: 'white',
                    borderRadius: 5,
                    alignItems: 'stretch',
                    marginHorizontal: 10,
                    marginTop:5,
                    marginBottom: 5,
                    paddingVertical: 5
                }}>

                    <Text style={{fontSize: 13, marginLeft: 10, color: 'gray'}}>
                        Examples:
                    </Text>

                    {
                        this.props.data.fullInfo.sentences.map((sentence, i) => (
                            <Text key={i} style={{fontSize: 14, marginLeft: 10, color: 'black'}}>
                                {sentence.sentence}
                            </Text>
                        ))
                    }

                </View>

                <View style={{
                    flex: 1,
                    flexDirection: 'column',
                    backgroundColor: 'white',
                    borderRadius: 5,
                    alignItems: 'stretch',
                    marginHorizontal: 10,
                    marginTop:5,
                    marginBottom: 5,
                    paddingVertical: 5
                }}>

                    <Text style={{fontSize: 13, marginLeft: 10, color: 'gray'}}>
                        Similar:
                    </Text>

                    {
                        this.props.data.fullInfo.similarWords.map((similar, i) => (
                            <Text key={i} style={{fontSize: 14, marginLeft: 10, color: 'black'}}>
                                {similar}
                            </Text>
                        ))
                    }

                </View>

                <Button key="1" title="Next" buttonStyle={{
                    backgroundColor: 'rgba(113, 154, 112, 1)',
                    borderRadius: 5,
                    marginVertical: 5
                }} onPress={this.props.onNext}/>

            </View>
        );
    }

    translationTo4WordsRender() {
        return (
            <View>

                <View style={styles.firstBlock}>

                    <Text style={{fontSize: 13, marginLeft: 10, color: 'gray'}}>
                        Choose correct word
                    </Text>

                    <Text style={{fontSize: 17, marginLeft: 10, color: 'black'}}>
                        {this.props.data.word.translation}
                    </Text>

                </View>

                <SelectiveButtons
                    falseValues={this.props.data.fullInfo.falseOptions}
                    value={this.props.data.word.word}
                    onComplete={(result) => this.chose(result)}/>

            </View>
        );
    }

    translationToWriteRender() {
        return (
            <View>

                <View style={styles.firstBlock}>

                    <Text style={{fontSize: 13, marginLeft: 10, color: 'gray'}}>
                        Write correct word
                    </Text>

                    <Text style={{fontSize: 17, marginLeft: 10, color: 'black'}}>
                        {this.props.data.word.translation}
                    </Text>

                </View>

                <SelectiveInput
                    value={this.props.data.word.word} editableFirst={3}
                    showDisabled={true}
                    onComplete={(result) => this.chose(result)}/>

            </View>
        );
    }

}