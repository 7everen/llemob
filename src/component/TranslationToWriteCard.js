import React, { Component } from "react";
import {Text, View, ScrollView, Animated, Keyboard, Platform, Dimensions} from 'react-native';
import ImageInCard from "./ImageInCard";
import OptionalButtons from "./OptionalButtons";
import SelectiveInput from "../component/SelectiveInput";

const SCREEN_HEIGHT = Dimensions.get('window').height;

export default class TranslationToWriteCard extends Component {

    static navigationOptions = {
        header: null
    }

    state = {
        cardHeight: 0
    };

    componentWillMount() {

        this.keyboardWillShowListener = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow)
        this.keyboardWillHideListener = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide)

        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow)
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide)

        this.cardVisibleHeight = new Animated.Value(0)
    }

    async componentDidMount(){

    }

    keyboardWillShow = (event) => {

        if(Platform.OS == 'android') {
            duration = 100
        }else{
            duration = event.duration
        }

        Animated.timing(this.cardVisibleHeight, {
            duration: duration + 100,
            toValue: SCREEN_HEIGHT - event.endCoordinates.height
        }).start()
    }

    keyboardWillHide = (event) => {
        if(Platform.OS == 'android') {
            duration = 100
        }else{
            duration = event.duration
        }

        Animated.timing(this.cardVisibleHeight, {
            duration: duration + 100,
            toValue: SCREEN_HEIGHT
        }).start()
    };

    render() {
        return (
            <Animated.ScrollView style={{height:this.cardVisibleHeight}}>
                <View style={{marginTop:40}}>

                    <ImageInCard base64={this.props.data.fullInfo.base64}/>

                    <View>

                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            backgroundColor: 'white',
                            borderRadius: 5,
                            alignItems: 'stretch',
                            marginHorizontal: 10,
                            marginTop:5,
                            marginBottom: 5,
                            paddingVertical: 5
                        }}>

                            <Text style={{fontSize: 13, marginLeft: 10, color: 'gray'}}>
                                Write correct word
                            </Text>

                            <Text style={{fontSize: 17, marginLeft: 10, color: 'black'}}>
                                {this.props.data.word.translation}
                            </Text>

                        </View>


                        <SelectiveInput
                            style={{flex: 1, marginTop: 40}}
                            value={this.props.data.word.word} editableFirst={3}
                            showDisabled={true}
                            onComplete={(result) => this.chose(result)}/>

                        <OptionalButtons
                            onLearned={() => this.props.onNext(null, true)}
                            onSkip={() => this.props.onNext(null)}/>

                    </View>

                </View>
            </Animated.ScrollView>

        );
    }

    chose(value) {
        if(value){
            this.props.onNext(value == this.props.data.word.word);
        }

    }

}