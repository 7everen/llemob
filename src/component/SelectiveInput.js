import React, { Component } from "react";
import {StyleSheet, View, TextInput} from 'react-native';

const styles = StyleSheet.create({
    input: {
        height: 40,
        marginHorizontal: 1,
        fontSize:18,
        paddingHorizontal:5,
        paddingVertical:0,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#666666',
        borderRadius: 0,
        width:25,
        height:30,
        color: 'black'},
    disabledInput: {
        backgroundColor: '#cccccc'
    },
    focusedInput: {
        borderColor: '#148bff'
    },
    doesntCorectInput: {
        borderColor: '#cf0200'
    },
    space: {
        width:25,
        height:30
    },
    view: {
        flex: 1,
        flexDirection: 'row',
        justifyContent:'center',
        paddingVertical:10
    }
});

const inputs = [];

export default class SelectiveInput extends Component {

    constructor(props) {
        super(props);

        this.state ={chars:[], isEditableChars:[], focusedIndex:-1, doesntCorrect:false};

        this.onFocus = this.onFocus.bind(this);
        this.textChanging = this.textChanging.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);

    }

    startValues;
    values;
    inputs;

    componentDidMount(){
        if(!this.props.editableFirst || this.props.editableFirst > this.props.value.length){
            this.props.editableFirst = this.props.value.length;
        }
        const chars = this.props.value.split("");
        const isEditableChars = new Array(this.props.value.length);
        isEditableChars.fill(false);
        const words = this.props.value.split(" ");

        this.inputs = [];
        this.startValues = new Array(this.props.value.length);
        this.values = new Array(this.props.value.length);

        let start = 0;
        for(let y = 0; y < words.length; y++) {
            let editable = words[y].length;
            if(this.props.editableFirst && this.props.editableFirst < editable) {
                editable = this.props.editableFirst;
            }
            if(y > 0){
                start++;
            }
            isEditableChars.fill(true, start, start + editable);
            for(let x = start; x < start + editable; x++) {
                this.startValues[x] = this.props.value[x];
            }
            start += words[y].length;
        }

        this.setState({
            chars: chars,
            isEditableChars: isEditableChars,
            focusedIndex: 0
        })
    }

    checkIfFilled() {
        for(let x = 0; x < this.inputs.length; x++) {
            if(this.inputs[x]){
                if(!this.values[x]){
                    return false;
                }
            }
        }
        return true;
    }

    getWrittenValue() {
        var value = "";
        for(let x = 0; x < this.state.isEditableChars.length; x++) {
            if(this.state.isEditableChars[x]){
                value += this.values[x];
            }else{
                value += this.props.value[x];
            }
        }
        return value;
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevState.focusedIndex !== this.state.focusedIndex){
            if(this.state.focusedIndex === -1) {
                this.inputs[prevState.focusedIndex].blur();
                if(this.checkIfFilled()){
                    let dc = this.props.value != this.getWrittenValue();
                    this.setState({doesntCorrect: dc})
                    if(!dc){
                        this.props.onComplete(this.props.value);
                    }
                }
            }else{
                this.inputs[this.state.focusedIndex].focus();
            }
        }
    }

    findLowestThan(i) {
        for(let y = i-1; y > -1; y--) {
            if(this.inputs[y]) return y;
        }
        return -1;
    }

    findHighestThan(i) {
        for(let y = i+1; y < this.inputs.length; y++) {
            if(this.inputs[y]) return y;
        }
        return -1;
    }

    onFocus(i) {
        if(this.state.focusedIndex !== i){
            this.setState({focusedIndex: i});
        }
    }

    textChanging(text, i) {
        this.values[i] = text;
        if(text.length === 1){
            if(i+1<this.inputs.length){
                this.setState({focusedIndex:this.findHighestThan(i)});
            }else{
                this.setState({focusedIndex:-1});
            }
        }else{
            this.setState({focusedIndex:this.findLowestThan(i)});
        }
    }

    handleKeyPress({ nativeEvent: { key: keyValue } }, i) {
        if (keyValue === 'Backspace') {
            if(i > 0){
                this.setState({focusedIndex:this.findLowestThan(i)});
            }
        }
    }

    render() {
        return (
            <View style={styles.view}>
                {
                    this.state.chars.map((char, i) => (
                        char === " "?
                            <View key={i} style={styles.space} />
                            :
                            (this.state.isEditableChars[i]?
                                <TextInput key={i} ref={(input) => { this.inputs[i] = input;}}
                                           onKeyPress={(e)=>this.handleKeyPress(e, i)}
                                           style={[styles.input, this.state.doesntCorrect && styles.doesntCorectInput, this.state.focusedIndex===i && styles.focusedInput]}
                                           maxLength={1}
                                           blurOnSubmit={false}
                                           onChangeText={(text) => this.textChanging(text, i)}
                                           onFocus={() => this.onFocus(i)}
                                           selectTextOnFocus={true}
                                           autoCapitalize = 'none'
                                />
                                :
                                <TextInput key={i}
                                           style={[styles.input, styles.disabledInput]}
                                           editable={false}
                                           value={this.props.showDisabled?char:""}
                                />)
                    ))
                }
            </View>
        )

    }

}