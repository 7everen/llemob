import React, { Component } from "react";
import {StyleSheet, View, TextInput, FlatList, Image, Dimensions, Text, TouchableOpacity} from 'react-native';
import Utils from "../service/Utils";
import DefaultImages from "../service/DefaultImages";

const imageWidth = Dimensions.get('window').width/2-40;

const styles = StyleSheet.create({
    button: {
        backgroundColor: 'rgba(113, 154, 112, 1)',
        borderRadius: 5,
        marginVertical: 5
    },
    image: {
        flex: 1,
        backgroundColor: '#ffffff',
        width:imageWidth,
        margin:5},
    doesntCorrectImage: {
        borderColor: '#cf0200',
        borderWidth: 1
    }
});



export default class SelectiveImages extends Component {


    componentWillMount(){
        const images = Utils.compareItemAndShuffleArray(this.props.falseImages, this.props.image);
        let imageIsRed = Array(images.length).fill(false);
        this.setState({
            imageIsRed: imageIsRed,
            images: images
        });
    }

    choose(image, index) {
        if(image == this.props.image) {
            this.props.onComplete(true);
        }else{
            this.state.imageIsRed[index] = true;
            this.setState({imageIsRed: this.state.imageIsRed});
        }
    }

    renderItem = ({item, index}) => {
        return (
            <View style={[styles.image, this.state.imageIsRed[index] && styles.doesntCorrectImage]}>
                <TouchableOpacity
                    onPress={() => this.choose(item, index)}>
                    <Image source={{"uri":item}} style={{flex:1,width:null, height:imageWidth/4*3}} resizeMode={'cover'}></Image>
                </TouchableOpacity>
            </View>
        );
    };

    render() {
        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
            }}>
                <FlatList
                    numColumns={2}
                    extraData={this.state}
                    data={this.state.images}
                    renderItem={this.renderItem}
                    keyExtractor={(index)=>{return index}}
                />

            </View>
        )

    }

}