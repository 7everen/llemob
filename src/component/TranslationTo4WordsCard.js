import React, { Component } from "react";
import {Text, View, ScrollView} from 'react-native';
import ImageInCard from "./ImageInCard";
import SelectiveButtons from "./SelectiveButtons";
import LearnManager from "../manager/LearnManager";
import OptionalButtons from "./OptionalButtons";

export default class TranslationTo4WordsCard extends Component {

    state = {
        cardHeight: 0
    };

    async componentDidMount(){

    }

    render() {
        return (
            <ScrollView>
                <View style={{marginTop:40}}>

                    <ImageInCard base64={this.props.data.fullInfo.base64}/>

                    <View>

                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            backgroundColor: 'white',
                            borderRadius: 5,
                            alignItems: 'stretch',
                            marginHorizontal: 10,
                            marginTop:5,
                            marginBottom: 5,
                            paddingVertical: 5
                        }}>

                            <Text style={{fontSize: 13, marginLeft: 10, color: 'gray'}}>
                                Choose correct word
                            </Text>

                            <Text style={{fontSize: 17, marginLeft: 10, color: 'black'}}>
                                {this.props.data.word.translation}
                            </Text>

                        </View>

                        <SelectiveButtons
                            falseValues={this.props.data.fullInfo.falseOptions}
                            value={this.props.data.word.word}
                            onComplete={(result) => this.chose(result)}/>

                        <OptionalButtons onLearned={() => this.props.onNext(null, true)}/>

                    </View>

                </View>
            </ScrollView>

        );
    }

    chose(value) {
        if(value){
            this.props.onNext(value == this.props.data.word.word);
        }

    }

}