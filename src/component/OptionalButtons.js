import React, {Component} from 'react';
import {StyleSheet, View, Alert} from 'react-native';
import {Button} from "react-native-elements";

const styles = StyleSheet.create({
    button: {
        borderRadius: 5,
        marginVertical: 5
    }
});

export default class OptionalButtons extends Component {

    onLearned() {
        Alert.alert(
            'Confirmation',
            'Do you want to check this word as completely learned?',
            [
                {
                    text: 'Cancel',
                    style: 'cancel',
                },
                {
                    text: 'OK',
                    onPress: () => this.props.onLearned()
                },
            ],
            {cancelable: false},
        );
    }

    render() {
        return (
            <View style={{
                marginTop: 25}}>
                <Button title="I don't remember this word" style={styles.button} onPress={() => this.props.onSkip()}/>
                <Button title="I have learned this word completely" style={styles.button} onPress={() => this.onLearned()}/>
            </View>
        );
    }

}