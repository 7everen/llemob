import React, { Component } from "react";
import {StyleSheet, View, TextInput} from 'react-native';
import {Button} from "react-native-elements";
import Utils from "../service/Utils";

const styles = StyleSheet.create({
    button: {
        backgroundColor: 'rgba(113, 154, 112, 1)',
        marginVertical: 5
    },
    buttonRed: {
        backgroundColor: 'rgba(215, 102, 70, 1)',
        marginVertical: 5
    }
});

export default class SelectiveButtons extends Component {

    componentWillMount() {
        let options = Utils.compareItemAndShuffleArray(this.props.falseValues, this.props.value);
        let btnIsRed = Array(options.length).fill(false);
        this.setState({
            options: options,
            btnIsRed: btnIsRed
        });
    }

    render() {
        return (
            <View>
                {
                    this.state.options.map((option, i) => option == this.props.value
                            ?
                            (<Button key={i} title={option} buttonStyle={styles.button} onPress={() => this.props.onComplete(this.props.value === option)}/>)
                            :
                            (<Button key={i} title={option} buttonStyle={this.state.btnIsRed[i]?styles.buttonRed:styles.button} onPress={() => {
                                this.state.btnIsRed[i] = true;
                                this.setState({btnIsRed: this.state.btnIsRed});
                            }}/>))
                }
            </View>
        )

    }

}