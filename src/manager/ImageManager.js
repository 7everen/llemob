import { AsyncStorage } from "react-native"
import Utils from "../service/Utils";
import DefaultImages from "../service/DefaultImages";
import Md5 from "../service/Md5";

export default class ImageManager {

    static myInstance = null;

    static IMAGE_PREFFIX_KEY = "image_";

    /**
     * @returns {ImageManager}
     */
    static getInstance() {
        if (ImageManager.myInstance == null) {
            ImageManager.myInstance = new ImageManager();
        }

        return this.myInstance;
    }

    async getRandomImages(exceptValue, count){
        if(!count) count = 3;
        let exceptMd5 = Md5.encode(exceptValue);
        let keys = await AsyncStorage.getAllKeys();
        console.log("[getRandomImages] count keys: " + keys.length);
        keys = Utils.shuffleArray(keys);
        let images = [];
        for(let i=0; i<keys.length; i++) {
            if(keys[i].indexOf(ImageManager.IMAGE_PREFFIX_KEY) === 0 && keys[i] != ImageManager.IMAGE_PREFFIX_KEY+exceptMd5){
                images.push(await AsyncStorage.getItem(keys[i]));
            }
            if(images.length == count){
                return images;
            }
        }

        if(images.length < count){
            let shuffled = Utils.shuffleArray(DefaultImages.LIST_FALSE_IMAGES);
            const needImages = count-images.length;
            for(let i=0; i<needImages; i++) {
                images.push(shuffled[i]);

            }
        }

        return images;
    }

    async setImage(word, base64){
        console.log("[setImage] with word: " + word);
        const md5 = Md5.encode(word);
        await AsyncStorage.setItem(ImageManager.IMAGE_PREFFIX_KEY+md5, base64);
    }

    async getImage(word){
        console.log("[getImage] with word: " + word);
        const md5 = Md5.encode(word);
        return await AsyncStorage.getItem(ImageManager.IMAGE_PREFFIX_KEY+md5);
    }


}