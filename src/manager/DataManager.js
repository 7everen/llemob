import ApiService from "../service/ApiService";
import {AsyncStorage} from "react-native";
import Utils from "../service/Utils";
import DefaultImages from "../service/DefaultImages";
import ImageManager from "./ImageManager";

export default class DataManager {

    static myInstance = null;
    static MAIN = "main";

    _userID = "";
    _hash = "";
    _localData;


    /**
     * @returns {DataManager}
     */
    static getInstance() {
        if (DataManager.myInstance == null) {
            DataManager.myInstance = new DataManager();
        }

        return this.myInstance;
    }

    getUserID() {
        return this._userID;
    }

    async getLocalData(){
        if(!this._localData){
            this._localData = await this.fetchLocalData();
        }
        return this._localData;
    }

    async fetchLocalData(){
        try {
            let data = await AsyncStorage.getItem(DataManager.MAIN);
            if (data !== null) {
                const _d = JSON.parse(data);
                if(_d.syncTo && _d.pages){
                    console.log("Local data has fetched");
                    return _d;
                }
            }
            console.log("Local data has created");
            return {
                syncTo: '1970-01-01T00:00:00',
                pages: []
            }
        } catch (error) {
            Utils.warning("Local storage Error:"+error);
        }
    }

    async persistLocalData(data){
        try {
            await AsyncStorage.setItem(DataManager.MAIN, JSON.stringify(data));
            console.log("Local data has saved");
        } catch (error) {
            Utils.warning("Local storage Error:"+error);
        }
    }

    async getHash(){
        if(!this._hash){
            const result = await ApiService.fetchProfile("dlazarenko.ua@gmail.com");
            console.log("fetchProfile: "+JSON.stringify(result));
            this._hash = result.hash;
        }
        return this._hash;
    }

    async fullWordInfo(word, from, to, pos, translation){
        const hash = await this.getHash()
        let result = await ApiService.fetchFullWordInfo(hash, word, from, to, pos, translation);
        if(result.errors){
            result = {
                "falseOptions": [],
                "falseTranslationOptions": [],
                "sentences": [],
                "similarWords": []
            };
        }
        let imageManager = ImageManager.getInstance();
        if(result.base64){
            await imageManager.setImage(word, result.base64);
        }
        if(!result.base64){
            result.base64 = DefaultImages.NOT_FOUND_IMAGE;
        }
        result.falseImages = await imageManager.getRandomImages(word);
        return result;
    }

    async sync(){
        const data = await this.getLocalData();
        const hash = await this.getHash();
        const result = await ApiService.fetchSync(hash, data.syncTo);
        console.log("fetchSync: "+JSON.stringify(result));

        if(result && result.pages && result.pages.length && result.syncTo){
            data.syncTo = result.syncTo;
            this.mergePages(data.pages, result.pages);
            await this.persistLocalData(data);
        }

    }

    mergePages(pages, newPages){
        newPages.forEach((newPage) => {
            let page = pages.find((page) => page.url === newPage.url);
            if(!page){
                page = {
                    "url": newPage.url,
                    "title": newPage.title,
                    "sentences": []
                };
                pages.push(page);
            }
            newPage.sentences.forEach((newSentence) => {
                let sentence = page.sentences.find((sentence) => newSentence.sentence === sentence.sentence);
                if(!sentence){
                    sentence = {
                        "from": newSentence.from,
                        "to": newSentence.to,
                        "extra": newSentence.extra,
                        "sentence": newSentence.sentence,
                        "words": []
                    };
                    page.sentences.push(sentence);
                }
                newSentence.words.forEach((newWord) => {
                    let word = sentence.words.find((word) => word.word === newWord.word);
                    if(!word){
                        sentence.words.push(newWord);
                    }else{
                        const i = sentence.words.indexOf(word);
                        sentence.words[i] = newWord;
                    }
                });
            });
        });
    }

    async getPages() {
        const data = await this.getLocalData();
        return data.pages;
        //const hash = await this.getHash();
        //const result = await ApiService.fetchSync(hash, '1970-01-01T00:00:00');
        //return result.pages;
    }

    setUserID(id) {
        this._userID = id;
    }
}