/** @format */

'use strict';


import Utils from "../service/Utils";
import ApiService from "../service/ApiService";
import DataManager from "./DataManager";
import DefaultImages from "../service/DefaultImages";

;

export default class LearnManager {

    static myInstance = null;

    static FULL_INFO = "FULL_INFO";
    static TRANSLATION_TO_4WORDS = "TRANSLATION_TO_4WORDS";
    static SPEECH_TO_4WORDS = "SPEECH_TO_4WORDS";
    static SPEECH_TO_4IMAGES = "SPEECH_TO_4IMAGES";
    static TRANSLATION_TO_WRITE = "TRANSLATION_TO_WRITE";
    static LEARN_TYPES = [
        LearnManager.TRANSLATION_TO_4WORDS,
        LearnManager.SPEECH_TO_4WORDS,
        LearnManager.SPEECH_TO_4IMAGES,
        LearnManager.TRANSLATION_TO_WRITE
    ];

    _pages;
    _wordsToLearn;
    _index;

    static getInstance() {
        if (LearnManager.myInstance == null) {
            LearnManager.myInstance = new LearnManager();
        }

        return this.myInstance;
    }

    constructor(){

    }

    start(pages) {
        this._pages = pages;
        this._index = -1;
        this._wordsToLearn = Utils.shuffleArray(Utils.mustLearn(this._pages));
    }

    hasNext(){
        if(!this._wordsToLearn) return false;
        const item = Utils.findNotLearnedWord(this._wordsToLearn, this._index + 1);
        return !!item;
    }

    async next(){
        this._index++;
        if(this._index >= this._wordsToLearn.length){
            this._index = 0;
        }
        const item = Utils.findNotLearnedWord(this._wordsToLearn, this._index);
        this._index = this._wordsToLearn.indexOf(item);
        if(!item.fullInfo){
            console.log("fetchFullWordInfo:"+item.word.word);
            let dataManager = DataManager.getInstance();
            item.fullInfo = await dataManager.fullWordInfo(item.word.word, item.word.from, item.word.to, item.word.partOfSpeech, item.word.translation);



        }

        //logic for type
        let choseType;
        do{
            choseType = LearnManager.LEARN_TYPES[parseInt(Math.random()*LearnManager.LEARN_TYPES.length)];
        }while(item.fullInfo.base64 == DefaultImages.NOT_FOUND_IMAGE && choseType == LearnManager.SPEECH_TO_4IMAGES);

        item.type = choseType;

        return item;
    }

    increaseRate() {
        if(this._index != -1){
            const item = this._wordsToLearn[this._index];
            console.log("check:"+item.type);
            if(item.type != LearnManager.FULL_INFO){
                item.word.rate += 0.1;
                console.log("item.word.rate += 0.1("+item.word.rate+")");
            }
        }
    }

    decreaseRate() {
        if(this._index != -1){
            const item = this._wordsToLearn[this._index];
            console.log("check:"+item.type);
            if(item.type != LearnManager.FULL_INFO){
                item.word.rate -= 0.1;
                if(item.word.rate<0) item.word.rate = 0;
                console.log("item.word.rate -= 0.1("+item.word.rate+")");
            }
        }
    }

    learned(){
        if(this._index != -1){
            const item = this._wordsToLearn[this._index];
            item.word.rate = 1;
        }
    }

    clear(){
        this._wordsToLearn = null;
        this._index = 0;
    }


};
