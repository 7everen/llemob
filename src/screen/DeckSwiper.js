import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ScrollView,
    Image,
    Text,
    Dimensions,
    PanResponder,
    Animated
} from 'react-native';

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;

const CARDS = [
    {id: "0", uri: require('llemob/images/0.jpg')},
    {id: "1", uri: require('llemob/images/1.jpg')},
    {id: "2", uri: require('llemob/images/2.jpg')},
    {id: "3", uri: require('llemob/images/3.jpg')},
];

export default class DeckSwiper extends Component {

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)

        this.position = new Animated.ValueXY()
        this.state = {
            currentIndex: 0
        }
    }

    componentWillMount() {
        this.panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (e, gestureState) => true,
            onPanResponderMove:(evt, gestureState) => {
                this.position.setValue({y:gestureState.dy})
            },
            onPanResponderRelease:(evt, gestureState) => {
                if(-gestureState.dy > 50 && -gestureState.vy > 0.7){
                    Animated.timing(this.position, {
                        toValue:({x:0, y:-SCREEN_HEIGHT}),
                        duration: 300
                    }).start(() => {
                        this.setState({currentIndex: this.state.currentIndex+1})
                        this.position.setValue({x: 0, y: 0})
                    })
                }else{
                    Animated.spring(this.position, {
                        toValue: ({x: 0, y: 0})
                    }).start()
                }
            }
        })
    }

    renderCards = () => {
        return CARDS.map((item, i) => {
            if(i < this.state.currentIndex){
                return null
            }
            if(i == this.state.currentIndex){

                return(
                    <Animated.View key={item.id} style={this.position.getLayout()}
                                   {...this.panResponder.panHandlers}
                    >
                        <View style={{flex:1, position: 'absolute', height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: 'white'}}>
                            <View style={{flex:2, backgroundColor: 'black'}}>
                                <Image source={item.uri}
                                style={{flex:1, height:null, width:null, resizeMode: 'contain'}}/>
                            </View>
                            <View style={{flex:3, padding: 5}}>
                                <Text>
                                    In this series we'll learn how to create the swipe deck animation of the inshorts app using the pan responder system of react native.
                                    In this series we'll learn how to create the swipe deck animation of the inshorts app using the pan responder system of react native.
                                    In this series we'll learn how to create the swipe deck animation of the inshorts app using the pan responder system of react native.
                                </Text>
                            </View>
                        </View>
                    </Animated.View>
                );
            } else {
                return (
                    <Animated.View key={item.id}
                    >
                        <View style={{flex:1, position: 'absolute', height: SCREEN_HEIGHT, width: SCREEN_WIDTH, backgroundColor: 'white'}}>
                            <View style={{flex:2, backgroundColor: 'black'}}>
                                <Image source={item.uri}
                                       style={{flex:1, height:null, width:null, resizeMode: 'contain'}}/>
                            </View>
                            <View style={{flex:3, padding: 5}}>
                                <Text>
                                    In this series we'll learn how to create the swipe deck animation of the inshorts app using the pan responder system of react native.
                                    In this series we'll learn how to create the swipe deck animation of the inshorts app using the pan responder system of react native.
                                    In this series we'll learn how to create the swipe deck animation of the inshorts app using the pan responder system of react native.
                                </Text>
                            </View>
                        </View>
                    </Animated.View>
                );
            }

        }).reverse()
    };

    render() {
        return (
            <View style={{flex:1 }}>
                {this.renderCards()}
            </View>
        );
    }

}