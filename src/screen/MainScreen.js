import React, { Component } from 'react';
import { ActivityIndicator, StyleSheet, Text, ScrollView, View } from "react-native";
import { Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/Ionicons';
import DataManager from "../manager/DataManager";
import Utils from "../service/Utils";
import LearnManager from "../manager/LearnManager";

const styles = StyleSheet.create({
    positiveView: {
        backgroundColor: 'rgba(220,230,218,1)',
        width: 70,
        height: 28,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 10
    },
    positiveText: {
        color: 'green',
        fontFamily: 'Montserrat-Regular',
        fontSize: 13,
        marginLeft: 5
    },
    negativeView: {
        backgroundColor: 'rgba(244,230,224,1)',
        width: 70,
        height: 28,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 10
    },
    negativeText: {
        color: 'red',
        fontFamily: 'Montserrat-Regular',
        fontSize: 13,
        marginLeft: 5
    },
    cardView: {
        height: 60,
        marginHorizontal: 10,
        marginTop: 10,
        backgroundColor: 'white',
        borderRadius: 5,
        alignItems: 'center',
        flexDirection: 'row'
    },
    cardTitleView: {
        flex: 2,
        flexDirection: 'row',
        alignItems: 'center'
    },
    h3: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 14,
        marginLeft: 10,
        color: 'black'
    },
    cardTitleText: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 14,
        marginLeft: 10,
        color: 'black'
    },
    cardButton: {
        backgroundColor: 'rgba(222,222,222,1)',
        width: 35,
        height: 28,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 10
    },
    cardButtonView: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginRight: 10
    },
    learnAllButton: {
        height: 50,
        width: 250,
        backgroundColor: 'rgba(113, 154, 112, 1)',
        borderRadius: 5,
        paddingVertical:1,
    },
    learnAllTitle: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 11,
        color: 'white'
    }
});

export default class PagesScreen extends Component {

    static navigationOptions = {
        title: 'Welcome',
    };

    constructor(props){
        super(props);
        this.state ={ isLoading: true}
    }

    async componentDidMount(){

        let dataManager = DataManager.getInstance();
        dataManager.setUserID("User1");
        await dataManager.sync();
        let pages = await dataManager.getPages();


        this.setState({
            isLoading: false,
            dataSource: pages
        });
    }

    render() {
        const { navigate } = this.props.navigation;

        if(this.state.isLoading){
            return(
                <View style={{flex: 1, padding: 20}}>
                    <ActivityIndicator/>
                </View>
            )
        }
        return (
            /*<Button
                title={"Go to Profile. Got param:" + this.props.navigation.getParam('name')}
                onPress={() =>
                    navigate('Profile', { name: 'H' })
                }
            />*/
            <ScrollView>
                <View>
                    <View style={{flex: 1, flexDirection: 'column', backgroundColor: 'white', borderRadius: 5, alignItems: 'center', marginHorizontal: 10, marginTop:10, height: 250, marginBottom: 10}}>


                        <Text style={styles.h3}>You Have learned:</Text>
                        <Text style={styles.h3}>Words in learning:</Text>
                        <Text style={styles.h3}>Your vocabularity:</Text>

                        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                            <Button
                                large
                                icon={{name: 'cached'}}
                                title='Remember My Words'
                                buttonStyle={styles.learnAllButton}
                                size={13}
                                titleStyle={{fontFamily: 'Montserrat-Regular', fontSize: 13, color: 'white'}}
                                onPress={() => navigate('Quest', { dataSource: this.state.dataSource})
                                }
                                underlayColor="transparent"
                            />
                        </View>
                    </View>
                    {
                        this.state.dataSource.map((page, i) => this.renderCard(page, i))
                    }
                </View>
            </ScrollView>
        );
    }

    subtitle(page){
        const toLearn = Utils.countWordsInPage(page, "ADD");
        const learned = Utils.countWordsInPage(page, "REMOVE");
        let str = toLearn + " word";
        if(toLearn != 1){
            str += "s";
        }
        if(learned){
            str += " / learned "+learned+" words";
        }
        return str;
    }

    renderValue(page) {
        const positive  = true;
        const value = Utils.countWordsInPage(page, "ADD");

        if (positive) {
            return (
                <View style={styles.positiveView}>
                    <Icon
                        name='md-arrow-dropup'
                        color='green'
                        size={25}
                    />
                    <Text style={styles.positiveText}>{value}</Text>
                </View>
            );
        } else {
            return (
                <View style={style.negativeView}>
                    <Icon
                        name='md-arrow-dropdown'
                        color='red'
                        size={25}
                    />
                    <Text style={style.negativeText}>{value}</Text>
                </View>
            );
        }
    }

    renderCard(page, index) {
        const { title } = page;

        return (
            <View key={index} style={styles.cardView}>
                <View style={styles.cardTitleView}>
                    <Text style={styles.cardTitleText}
                    numberOfLines={2}
                    onPress={() => this.props.navigation.navigate('Page', { data: page })}>
                        {title}
                    </Text>
                </View>
                <View style={styles.cardButtonView}>
                    {this.renderValue(page)}
                    <View style={styles.cardButton}>
                        <Icon
                            name='md-person-add'
                            color='gray'
                            size={20}
                        />
                    </View>
                </View>
            </View>
        );
    }



}

;