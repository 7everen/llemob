import React, { Component } from 'react';
import {ActivityIndicator, Button, ScrollView, StyleSheet, Text, View} from "react-native";
import Utils from "../service/Utils";
import ImageToWordsCard from "../component/ImageToWordsCard";
import ApiService from "../service/ApiService";
import LearnManager from "../manager/LearnManager";
import TranslationTo4WordsCard from "../component/TranslationTo4WordsCard";
import TranslationToWriteCard from "../component/TranslationToWriteCard";
import SpeechTo4TranslationsCard from "../component/SpeechTo4TranslationsCard";
import SpeechTo4ImagesCard from "../component/SpeechTo4ImagesCard";

const styles = StyleSheet.create({
    cardView: {
        height: 60,
        marginHorizontal: 10,
        marginTop: 10,
        backgroundColor: 'white',
        borderRadius: 5,
        alignItems: 'center',
        flexDirection: 'row'
    },
    cardTitleView: {
        flex: 1,
        flexDirection: 'column'
    },
    cardTitleText: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 14,
        marginLeft: 10,
        color: 'black'
    },
    cardSubtitleText: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 13,
        marginLeft: 10,
        color: 'grey'
    },
    learnAllTitle: {
        fontFamily: 'Montserrat-Regular',
        color: 'white'
    }
});

export default class CardScreen extends Component {

    static navigationOptions = {
        header: null
    }

    state ={ isLoading: true, completed: false};

    async componentDidMount(){
        const manager = LearnManager.getInstance();
        const dataSource = this.props.navigation.getParam('dataSource');
        if(dataSource){
            manager.start(dataSource);
        }
        let wordData;
        if(manager.hasNext()){
            wordData = await manager.next();
        }

        this.setState({
            isLoading: false,
            wordData: wordData,
        });
    }

    async onNext(isCorrect, learned) {
        console.log("onNext");
        const manager = LearnManager.getInstance();
        if(learned){
            manager.learned();
        }else if(isCorrect) {
            manager.increaseRate();
        }else{
            manager.decreaseRate();
        }

        this.setState({
            isLoading: true
        });

        let wordData;
        if(manager.hasNext()){
            wordData = await manager.next();
        }
        this.setState({
            isLoading: false,
            wordData: wordData,
        });
    };

    render() {
        //const { navigate } = this.props.navigation;

        if(this.state.isLoading){
            return(
                <View style={{flex: 1, padding: 20}}>
                    <ActivityIndicator/>
                </View>
            )
        }
        if(this.state.wordData){

            let card;

            switch (this.state.wordData.type) {
                case LearnManager.TRANSLATION_TO_4WORDS:
                    card = <TranslationTo4WordsCard data={this.state.wordData} onNext={(isCorrect, learned) => this.onNext(isCorrect, learned)}/>;
                    break;
                case LearnManager.TRANSLATION_TO_WRITE:
                    card = <TranslationToWriteCard data={this.state.wordData} onNext={(isCorrect, learned) => this.onNext(isCorrect, learned)}/>;
                    break;
                case LearnManager.SPEECH_TO_4WORDS:
                    card = <SpeechTo4TranslationsCard data={this.state.wordData} onNext={(isCorrect, learned) => this.onNext(isCorrect, learned)}/>;
                    break;
                case LearnManager.SPEECH_TO_4IMAGES:
                    card = <SpeechTo4ImagesCard data={this.state.wordData} onNext={(isCorrect, learned) => this.onNext(isCorrect, learned)}/>;
                    break;
            }

            return (
                <View>
                    { card }
                </View>
            );

            /*return (
                <View>
                    <ImageToWordsCard data={this.state.wordData} onNext={(value) => this.onNext(value)}/>
                </View>
            );*/
        }else{
            return (
                <View>
                    <Text>All Words Learned</Text>
                </View>
            );
        }

    }
}