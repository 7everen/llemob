import React, { Component } from 'react';
import {ActivityIndicator, StyleSheet, Text, ScrollView, View, TouchableOpacity, TextInput} from "react-native";
import { Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/Ionicons';
import DataManager from "../manager/DataManager";
import Utils from "../service/Utils";
import LearnManager from "../manager/LearnManager";
import SelectiveInput from "../component/SelectiveInput";

const styles = StyleSheet.create({

});

export default class PagesScreen extends Component {

    static navigationOptions = {
        header: null
    };

    constructor(props){
        super(props);
        this.state ={
            isLoading: false,
            value: ""
        }
    }

    async componentDidMount(){

        /*this.setState({
            isLoading: false,
            dataSource: pages
        });*/
    }


    render() {
        const { navigate } = this.props.navigation;

        if(this.state.isLoading){
            return(
                <View style={{flex: 1, padding: 20}}>
                    <ActivityIndicator/>
                </View>
            )
        }
        return (
            <ScrollView>
                <View style={{marginTop:100}}>
                    <SelectiveInput
                        style={{flex: 1, padding: 20}}
                        value="apple juice" editableFirst={2}
                        showDisabled={true}
                        onComplete={(result) => console.log("Selective input:"+result)}/>
                </View>
            </ScrollView>
        );
    }

}

;