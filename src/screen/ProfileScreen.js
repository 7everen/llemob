import React, { Component } from 'react';
import {Button} from "react-native";

export default class ProfileScreen extends Component {

    static navigationOptions = {
        title: 'Profile',
    };
    render() {
        const { navigate } = this.props.navigation;
        return (
            <Button
                title={"Go to Home. Param from last screen:" + this.props.navigation.getParam('name')}
                onPress={() =>
                    navigate('Pages', { name: 'Profile Data' })
                }
            />
        );
    }
}