import React, { Component } from 'react';
import {Button, ScrollView, StyleSheet, Text, View} from "react-native";
import Utils from "../service/Utils";

const styles = StyleSheet.create({
    cardView: {
        height: 60,
        marginHorizontal: 10,
        marginTop: 10,
        backgroundColor: 'white',
        borderRadius: 5,
        alignItems: 'center',
        flexDirection: 'row'
    },
    cardTitleView: {
        flex: 1,
        flexDirection: 'column'
    },
    cardTitleText: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 14,
        marginLeft: 10,
        color: 'black'
    },
    cardSubtitleText: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 13,
        marginLeft: 10,
        color: 'grey'
    },
    learnAllTitle: {
        fontFamily: 'Montserrat-Regular',
        color: 'white'
    }
});

export default class PageScreen extends Component {

    /*static navigationOptions = {
        title: 'Page',
    };*/
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View>
                    <View style={{flex: 1, flexDirection: 'column', backgroundColor: 'white', borderRadius: 5, alignItems: 'center', marginHorizontal: 10, marginTop:10, height: 50, marginBottom: 10}}>
                        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                            <Button
                                title='Learn All'
                                buttonStyle={styles.learnAllButton}
                                titleStyle={{fontFamily: 'Montserrat-Regular', fontSize: 13, color: 'white'}}
                                onPress={() => navigate('Page', { name: "", pages: this.state.dataSource })}
                                underlayColor="transparent"
                            />
                        </View>
                    </View>
                    {
                        Utils.wordsInPage(this.props.navigation.getParam('data')).map((word, i) => (
                            <View key={i} style={styles.cardView}>
                                <View style={styles.cardTitleView}>
                                    <Text style={styles.cardTitleText}
                                          numberOfLines={1}
                                          onPress={() => navigate('Quest', { data: word })}>
                                        {word.word}
                                    </Text>
                                    <Text style={styles.cardSubtitleText}
                                          numberOfLines={1}
                                          onPress={() => navigate('Quest', { data: word })}>
                                        {word.translation}
                                    </Text>
                                </View>
                            </View>
                        ))
                    }
                </View>
            </ScrollView>

        );
    }
}