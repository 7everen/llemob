import React, {Component} from 'react';
import {StyleSheet, View, ScrollView, Image, Text, Animated} from 'react-native';

HEADER_MAX_HEIGHT = 120;
HEADER_MIN_HEIGHT = 70;
PROFILE_IMAGE_MAX_HEIGHT = 80;
PROFILE_IMAGE_MIN_HEIGHT = 40;

export default class Test2Screen extends Component {

    static navigationOptions = {
        header: null
    }

    state = {scrollY: new Animated.Value(0)}

    render() {

        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
            extrapolate: "clamp"
        });

        const profileImageHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [PROFILE_IMAGE_MAX_HEIGHT, PROFILE_IMAGE_MIN_HEIGHT],
            extrapolate: "clamp"
        });

        const profileImageMarginTop = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [HEADER_MAX_HEIGHT-PROFILE_IMAGE_MAX_HEIGHT/2, HEADER_MAX_HEIGHT + 5],
            extrapolate: "clamp"
        });

        const headerZindex = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [0,1],
            extrapolate: "clamp"
        });

        const headerTitleBottom = this.state.scrollY.interpolate({
            inputRange: [
                0,
                HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT + PROFILE_IMAGE_MIN_HEIGHT - 10,//HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT + PROFILE_IMAGE_MIN_HEIGHT + 5  in ios
                HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT + PROFILE_IMAGE_MIN_HEIGHT + 26
            ],
            outputRange: [-20, -20, 5],//[-20, -20, 0] in ios
            extrapolate: "clamp"
        });

        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <Animated.View style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    backgroundColor: 'lightskyblue',
                    height: headerHeight,
                    zIndex: headerZindex,
                    alignItems: 'center'
                }}>
                    <Animated.View style={{position: 'absolute', bottom: headerTitleBottom}}>
                        <Text style={{textShadowRadius: 1, color: 'white', fontSize:14, fontWeight:'bold'}}>Dmytro Lazarenko</Text>
                    </Animated.View>
                </Animated.View>

                <ScrollView style={{ flex: 1 }}
                            scrollEventThrottle={16}
                            onScroll={Animated.event(
                                [{nativeEvent:{contentOffset:{y:this.state.scrollY}}}]
                            )}>
                    <Animated.View style={{
                        height: profileImageHeight,
                        width: profileImageHeight,
                        borderRadius: PROFILE_IMAGE_MAX_HEIGHT/2,
                        borderColor: 'white',
                        borderWidth: 3,
                        overflow: 'hidden',
                        marginTop: profileImageMarginTop,
                        marginLeft: 10
                    }}>
                        <Image source={require('llemob/assets/me.jpg')}
                            style={{
                                flex: 1,
                                width: null,
                                height: null
                            }}
                        />
                    </Animated.View>
                    <View style={{}}>
                        <Text style={{
                            fontWeight: 'bold',
                            fontSize: 20,
                            paddingLeft: 10
                        }}>
                            Dmytro Lazarenko
                        </Text>
                    </View>

                    <Text style={{fontSize: 50, height: 1000}}>AAA</Text>
                </ScrollView>


            </View>

        );
    }
}