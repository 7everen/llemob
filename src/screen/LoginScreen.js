import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    ImageBackground,
    TextInput,
    TouchableOpacity,
    Animated,
    Dimensions,
    Keyboard,
    Platform
} from 'react-native';

import * as Animatable from 'react-native-animatable'
import {Icon} from 'native-base'

const SCREEN_HEIGHT = Dimensions.get('window').height;

export default class LoginScreen extends Component {

    static navigationOptions = {
        header: null
    }

    componentWillMount() {
        this.loginHeight = new Animated.Value(150)

        this.keyboardWillShowListener = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow)
        this.keyboardWillHideListener = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide)

        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow)
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide)

        this.cardVisibleHeight = new Animated.Value(0)
        this.forwardArrowOpacity = new Animated.Value(0)
        this.borderBottomWidth = new Animated.Value(0)

    }

    keyboardWillShow = (event) => {

        if(Platform.OS == 'android') {
            duration = 100
        }else{
            duration = event.duration
        }

        Animated.parallel([
            Animated.timing(this.cardVisibleHeight, {
                duration: duration + 100,
                toValue: event.endCoordinates.height + 10
            }),

            Animated.timing(this.forwardArrowOpacity, {
                duration: duration,
                toValue: 1
            }),

            Animated.timing(this.borderBottomWidth, {
                duration: duration,
                toValue: 1
            })
        ]).start()
    }

    keyboardWillHide = (event) => {
        if(Platform.OS == 'android') {
            duration = 100
        }else{
            duration = event.duration
        }

        Animated.parallel([
            Animated.timing(this.cardVisibleHeight, {
                duration: duration + 100,
                toValue: 0
            }),

            Animated.timing(this.forwardArrowOpacity, {
                duration: duration,
                toValue: 0
            }),

            Animated.timing(this.borderBottomWidth, {
                duration: duration,
                toValue: 0
            })
        ]).start()
    };

    increaseHeightOfLogin = () => {
        /*Keyboard.dismiss();*/
        Animated.timing(this.loginHeight, {
            toValue: SCREEN_HEIGHT,
            duration: 500
        }).start(() => {
            this.refs.textInputMobile.focus()
        })
    };

    decreaseHeightOfLogin = () => {
        Keyboard.dismiss();
        Animated.timing(this.loginHeight, {
            toValue: 150,
            duration: 500
        }).start()
    };

    render() {

        const headerTextOpacity = this.loginHeight.interpolate({
            inputRange: [150, SCREEN_HEIGHT],
            outputRange: [1, 0]
        });

        const marginTop = this.loginHeight.interpolate({
            inputRange: [150, SCREEN_HEIGHT],
            outputRange: [25, 100]
        });

        const headerBackArrowOpacity = this.loginHeight.interpolate({
            inputRange: [150, SCREEN_HEIGHT-200, SCREEN_HEIGHT],
            outputRange: [0, 0, 1]
        });

        return (
            <View style={{ flex: 1 }}>

                <Animated.View style={{
                    position: 'absolute',
                    height: 60,
                    width: 60,
                    top: 60,
                    left: 25,
                    zIndex: 100,
                    opacity: headerBackArrowOpacity
                }}>
                    <TouchableOpacity
                        onPress={() => this.decreaseHeightOfLogin()}
                    >
                        <Icon name="md-arrow-back" style={{color: 'black'}}/>
                    </TouchableOpacity>
                </Animated.View>

                <Animated.View style={{
                    position: 'absolute',
                    height: 60,
                    width: 60,
                    right: 10,
                    bottom: this.cardVisibleHeight,
                    opacity: this.forwardArrowOpacity,
                    zIndex: 100,
                    backgroundColor: '#54575e',
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 30
                }}>

                    <Icon name="md-arrow-forward" style={{color: 'white'}}/>
                </Animated.View>

                <ImageBackground
                source={require('llemob/assets/images/login_bg2.jpg')}
                style={{ flex: 1 }}>
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <Animatable.View
                            animation="zoomIn" iterationCount={1}
                            style={{backgroundColor: 'white',
                            height: 100, width:200,
                            justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{fontSize: 26, color: 'black'}}>Grizzly in mind</Text>
                        </Animatable.View>

                    </View>


                    <Animatable.View animation="slideInUp" iterationCount={1}>

                        <Animated.View style={{
                            height: this.loginHeight,
                            backgroundColor: 'white'}}>

                            <Animated.View style={{
                                opacity: headerTextOpacity,
                                alignItems: 'flex-start',
                                paddingHorizontal: 25,
                                marginTop: marginTop
                            }}>

                                <Text style={{fontSize: 20, color: 'black'}}>
                                    Start to put Grizzly-words in mind
                                </Text>

                            </Animated.View>
                            <TouchableOpacity
                                onPress={()=>this.increaseHeightOfLogin()}
                            >
                                <Animated.View
                                    pointerEvents="none"
                                    style={{
                                    marginTop: 25,
                                    paddingHorizontal: 25,
                                    flexDirection: 'row',
                                    borderBottomWidth: this.borderBottomWidth
                                }}>
                                    {/*<Image source={require('llemob2/assets/images/india.png')}
                                           style={{height: 24, width:24, resizeMode: 'contain'}}/>*/}

                                    {/*<View style={{flexDirection: 'row', flex: 1}}>

                                        <Text style={{ fontSize:16, paddingHorizontal: 10, color: 'black'}}>+380</Text>



                                    </View>*/}

                                    <TextInput ref="textInputMobile"
                                               style={{flex:1, fontSize: 16, paddingHorizontal: 10}}
                                               placeholder="Enter keyword from browser extension"
                                               underlineColorAndroid="transparent"/>

                                </Animated.View>

                            </TouchableOpacity>

                        </Animated.View>

                        <View style={{height: 70,
                        backgroundColor: 'white',
                        alignItems: 'flex-start',
                        justifyContent: 'center',
                        borderTopColor: '#e8e8e8',
                        borderTopWidth: 1,
                        paddingHorizontal: 25}}>
                            <Text style={{color: '#5a7fdf'}}>
                                What is the 'keyword'?
                            </Text>
                        </View>

                    </Animatable.View>
                </ImageBackground>
            </View>

        );
    }

}